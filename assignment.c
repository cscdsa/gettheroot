#include <stdio.h>
#include <math.h>

void oneRoot(double num1, double num2){
    double root=(num2*-1)/(num1*2);
    printf("The repeated root of the equation is %.3lf\n", root);
}
void twoRoots(double a, double b, double discr) {
    double sqroot = sqrt(discr);
    double root1 = ((b*-1)-sqroot)/(2*a);
    double root2 = ((b*-1)+sqroot)/(2*a);
    printf("The roots of the equation are %.3lf and %.3lf.\n", root1, root2);
}
double checkDiscriminant(double a, double b, double c) {
    return b*b-4*a*c;
}

void main() {
    double a, b, c;
    a = 0;
    while (!a) {
    printf("Enter coefficient of x^2: ");
    scanf("%lf", &a);
    }
    printf("Enter coefficient of x: ");
    scanf("%lf", &b);
    printf("Enter value of constant: ");
    scanf("%lf", &c);

    double discrim_val = checkDiscriminant(a,b,c);
    if (discrim_val<0) {
        printf("Complex-number roots!\n");
    } else if (discrim_val==0) {
        oneRoot(a,b);
    } else {
        twoRoots(a,b,discrim_val);
    }
}









